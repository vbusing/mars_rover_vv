package kata.mars.rover.Mars.Rover.test.motherfactory;

import kata.mars.rover.Mars.Rover.model.Obstacle;

import java.util.Arrays;
import java.util.List;

public class ObstaclesMother {
    public static List<Obstacle> getObstacles() {
        Obstacle forwardObstacle = new Obstacle(10,17);
        Obstacle backwardObstacle = new Obstacle(10,13);
        return Arrays.asList(forwardObstacle, backwardObstacle);
    }
}
