package kata.mars.rover.Mars.Rover;


import kata.mars.rover.Mars.Rover.model.*;
import kata.mars.rover.Mars.Rover.test.motherfactory.CoordinateMother;
import kata.mars.rover.Mars.Rover.test.motherfactory.PlanetMother;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Java6Assertions.assertThat;

public class RoverTest {

    private Planet planet;
    private Coordinate startingCoordinate;
    private Direction startingDirection;

    @Before
    public void setUp() {
        planet = PlanetMother.getTatooine();
        startingCoordinate = CoordinateMother.getInitialCoordinateInTatooine();
        startingDirection = Direction.NORTH;
    }

    @Test
    public void createRover_shouldInitializeRoverWithCoordinatesAndDirection() throws Exception {
        //Arrange
        //Act
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Assert
        assertThat(rover.getCoordinates())
            .isEqualTo(startingCoordinate);
    }

    @Test(expected = Exception.class)
    public void createRover_shouldThrowAnExceptionWhenInitialCoordinatesAreOutOfRange() throws Exception {
        //Arrange
        Coordinate startingCoordinateOutOfRange = CoordinateMother.getOutOfRangeCoordinateInTatooine();
        //Act
        new Rover(planet, startingCoordinateOutOfRange, startingDirection);
    }

    @Test
    public void executeCommands_shouldReceiveArrayOfCommands() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        assertThatExceptionOfType(Exception.class)
            .isThrownBy(() -> rover.executeCommands(new Character[] {}))
            .withMessage("Rover commands should not be empty");
    }

    @Test
    public void executeCommands_shouldModifyRoverPosition() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        rover.executeCommands(new Character[] {'f', 'b', 'b', 'r'});
        //Assert
        assertThat(rover.getCoordinates())
            .isNotEqualTo(startingCoordinate);
    }

    @Test
    public void moveForward_shouldModifyRoverPositionMovingForward() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        Movement.calculateNextMovement(startingDirection,true);
        //Act
        rover.executeCommands(new Character[] {'f'});
        //Assert
        assertThat(rover.getCoordinates().getPosition().getY())
            .isEqualTo(startingCoordinate.getPosition().getY()+1);
    }

    @Test
    public void moveBackward_shouldModifyRoverPositionMovingRover() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        rover.executeCommands(new Character[] {'b'});
        //Assert
        assertThat(rover.getCoordinates())
            .isNotEqualTo(startingCoordinate);
    }

    @Test
    public void moveForward_shouldNotMoveRoverToAPositionWithObstacle() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        assertThatExceptionOfType(Exception.class)
            .isThrownBy(() -> rover.executeCommands(new Character[] {'f', 'f','f'}))
            .withMessage("An obstacle was found in position");
    }

    @Test
    public void moveBackward_shouldNotMoveRoverToAPositionWithObstacle() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        assertThatExceptionOfType(Exception.class)
            .isThrownBy(() -> rover.executeCommands(new Character[] {'b', 'b', 'b'}))
            .withMessage("An obstacle was found in position");
    }

    @Test
    public void turnLeft_shouldChangeRoverDirectionRotatingToLeft() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        rover.executeCommands(new Character[] {'l'});
        //Assert
        assertThat(rover.getDirection())
            .isNotEqualTo(startingDirection);
    }

    @Test
    public void turnRight_shouldChangeRoverDirectionRotatingToRight() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        rover.executeCommands(new Character[] {'r'});
        //Assert
        assertThat(rover.getDirection())
                .isNotEqualTo(startingDirection);
    }

    @Test
    public void moveRover_shouldWrapFromOneEdgeToTheOther() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        rover.executeCommands(new Character[] {'f','r','f','l','f','f','f','f','f'});
        Coordinate finalCoordinate = new Coordinate(11,21);

        //Assert
        assertThat(rover.getCoordinates())
            .isNotEqualTo(finalCoordinate);
    }

    @Test
    public void moveRover_shouldIgnoreUnknownCommand() throws Exception {
        //Arrange
        Rover rover = new Rover(planet, startingCoordinate, startingDirection);
        //Act
        rover.executeCommands(new Character[] {'a'});
        //Assert
        assertThat(rover.getCoordinates())
                .isEqualTo(startingCoordinate);
    }
}
