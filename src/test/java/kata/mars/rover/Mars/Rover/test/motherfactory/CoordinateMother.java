package kata.mars.rover.Mars.Rover.test.motherfactory;

import kata.mars.rover.Mars.Rover.model.Coordinate;

public class CoordinateMother {

    public static final Coordinate getInitialCoordinateInTatooine() {
        return new Coordinate(10, 15);
    }

    public static final Coordinate getOutOfRangeCoordinateInTatooine() {
        return new Coordinate(21, 22);
    }
}
