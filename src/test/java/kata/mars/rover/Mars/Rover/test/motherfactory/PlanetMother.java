package kata.mars.rover.Mars.Rover.test.motherfactory;

import kata.mars.rover.Mars.Rover.model.Planet;

public class PlanetMother {
    public static Planet getTatooine(){
        return new Planet("Tatooine", 20,20, ObstaclesMother.getObstacles());
    }
}
