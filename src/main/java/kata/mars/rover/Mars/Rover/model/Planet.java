package kata.mars.rover.Mars.Rover.model;

import java.awt.*;
import java.util.List;

public class Planet {

    private String name;
    private int width;
    private int height;
    private List<Obstacle> obstacles;


    public Planet(String name, int width, int height, List<Obstacle> obstacles) {
        this.name = name;

        this.width = width;
        this.height = height;

        this.obstacles = obstacles;
    }

    public boolean hasObstacle(Point position) {
        return obstacles.contains(new Obstacle(position.x, position.y));
    }

    public boolean isOutOfBounds(Point position) {
        return position.getX() > width || position.getX() < 0 || position.getY() > height || position.getY() < 0;
    }

    public Point getOutOfBoundsNewPosition(Point nextPosition) {
        Point newPosition = new Point();

        if(nextPosition.getX() > width) {
            newPosition.x = 0;
            newPosition.y = nextPosition.y;
        }

        if(nextPosition.getX() < 0) {
            newPosition.x = width;
            newPosition.y = nextPosition.y;
        }

        if(nextPosition.getY() > height) {
            newPosition.x = nextPosition.x;
            newPosition.y = 0;
        }

        if(nextPosition.getY() < 0) {
            newPosition.x = nextPosition.x;
            newPosition.y = height;
        }

        return newPosition;
    }
}
