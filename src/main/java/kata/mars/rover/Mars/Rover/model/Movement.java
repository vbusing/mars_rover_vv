package kata.mars.rover.Mars.Rover.model;

public class Movement {
    public static Coordinate calculateNextMovement(Direction direction, boolean forward) {
        Coordinate nextPosition = new Coordinate(0,0);

        switch(direction) {
            case NORTH:
                nextPosition.getPosition().y = (forward? 1: -1);
                break;
            case SOUTH:
                nextPosition.getPosition().y = (forward? -1: 1);
                break;
            case EAST:
                nextPosition.getPosition().x = (forward? 1: -1);
                break;
            case WEST:
                nextPosition.getPosition().x = (forward? -1: 1);
                break;
        }
        return nextPosition;
    }
}
