package kata.mars.rover.Mars.Rover.model;

public enum Direction {
    NORTH("N"),
    SOUTH("S"),
    EAST("E"),
    WEST("W");

    private final String value;

    Direction(String value){
        this.value = value;
    }

    public final char direction() {
        return this.value.charAt(0);
    }

    public Direction turnRight() {
        switch(this) {
            case NORTH:
                return Direction.EAST;
            case SOUTH:
                return Direction.WEST;
            case EAST:
                return Direction.SOUTH;
            default:
                return Direction.NORTH;
        }
    }

    public Direction turnLeft() {
        switch(this) {
            case NORTH:
                return Direction.WEST;
            case SOUTH:
                return Direction.EAST;
            case EAST:
                return Direction.NORTH;
            default:
                return Direction.SOUTH;
        }
    }
}
