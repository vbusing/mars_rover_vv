package kata.mars.rover.Mars.Rover.model;

import java.awt.*;
import java.util.Objects;

public class Coordinate {
    private Point position;

    public Coordinate(Coordinate original) {
        this.position = new Point(original.position.x, original.position.y);
    }

    public Coordinate(int x, int y) {
        this.position = new Point(x, y);
    }

    public Point getPosition() {
        return this.position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coordinate that = (Coordinate) o;
        return Objects.equals(position.x, that.position.x) &&
            Objects.equals(position.y, that.position.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }
}
