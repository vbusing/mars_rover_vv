package kata.mars.rover.Mars.Rover.model;

import java.awt.*;
import java.util.Objects;

public class Obstacle {
    private Point position = new Point();

    public Obstacle(int x, int y) {
        this.position.x = x;
        this.position.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Obstacle obstacle = (Obstacle) o;
        return Objects.equals(position, obstacle.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position);
    }
}
