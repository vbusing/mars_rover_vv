package kata.mars.rover.Mars.Rover.model;

public enum Command {
    FORWARD("f"),
    BACKWARD("b"),
    LEFT("l"),
    RIGHT("r");

    private final String value;

    Command(String value) {
        this.value = value;
    }

    public final char command() {
        return this.value.charAt(0);
    }
}
