package kata.mars.rover.Mars.Rover.model;

import java.awt.*;

public class Rover {
    private Coordinate coordinate;
    private Planet planet;
    private Direction direction;

    public Rover(Planet planet, Coordinate startingCoordinate, Direction startingDirection) throws Exception {

        if (planet.isOutOfBounds(startingCoordinate.getPosition())) {
            throw new Exception("The rover cannot land on the planet because is out of range!");
        }

        this.planet = planet;
        this.coordinate = new Coordinate(startingCoordinate);
        this.direction = startingDirection;
    }

    public Direction getDirection() {
        return direction;
    }

    public Coordinate getCoordinates() {
        return this.coordinate;
    }

    public void executeCommands(Character[] commands) throws Exception {
        if(commands.length == 0) {
            throw new Exception("Rover commands should not be empty");
        }

        for (Character command : commands) {
            executeCommand(command);
        }
    }

    private void executeCommand(Character command) throws Exception {

        if(command.equals(Command.FORWARD.command())) {
            moveForward();
        }

        if(command.equals(Command.BACKWARD.command())) {
            moveBackward();
        }

        if(command.equals(Command.LEFT.command())) {
            turnLeft();
        }

        if(command.equals(Command.RIGHT.command())) {
            turnRight();
        }
    }

    private void turnRight() {
        direction = direction.turnRight();
    }

    private void turnLeft() {
        direction = direction.turnLeft();
    }

    private void moveBackward() throws Exception {
        Coordinate shiftPosition = Movement.calculateNextMovement(direction, false);
        checkMovement(shiftPosition);
    }

    private void moveForward() throws Exception {
        Coordinate shiftPosition = Movement.calculateNextMovement(direction, true);
        checkMovement(shiftPosition);
    }

    private void checkMovement(Coordinate shiftPosition) throws Exception {
        Point nextPosition = new Point(coordinate.getPosition().x, coordinate.getPosition().y);
        nextPosition.translate(shiftPosition.getPosition().x, shiftPosition.getPosition().y);
        if(planet.isOutOfBounds(nextPosition)) {
            nextPosition = planet.getOutOfBoundsNewPosition(nextPosition);
        }
        if (!planet.hasObstacle(nextPosition)) {
            coordinate.getPosition()
                .move(nextPosition.x, nextPosition.y);
        } else {
            throw new Exception ("An obstacle was found in position");
        }
    }
}

